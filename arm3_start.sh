#!/bin/bash

KERNEL="${HOME}/repos/kernels/linux/arch/arm/boot/bzImage"
QEMU_PATH="${HOME}/repos/emulators/qemu-2.12.0/arm-softmmu"
HDISK_PATH="${HOME}/repos/rootfs/arm/output/images"

${QEMU_PATH}/qemu-system-arm \
	-M vexpress-a9 \
	-smp 3 \
	-m 256 \
	-cpu cortex-a57 \
	-dtb ${HDISK_PATH}/vexpress-v2p-ca9.dtb \
	-kernel ${KERNEL} \
	-initrd ${HDISK_PATH}/rootfs.cpio \
	-append "console=ttyAMA0,115200" \
	-nographic \
	$1 $2
