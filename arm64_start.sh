#!/bin/bash

KERNEL="${HOME}/repos/kernels/linux/arch/arm64/boot/bzImage"
QEMU_PATH="${HOME}/repos/emulators/qemu-2.12.0/aarch64-softmmu"
HDISK_PATH="${HOME}/repos/rootfs/aarch64/output/images"

${QEMU_PATH}/qemu-system-aarch64 \
	-machine virt \
	-cpu cortex-a57 \
	-nographic -smp 1 \
	-hda ${HDISK_PATH}/rootfs.ext2 \
	-kernel ${KERNEL} \
	-append "root=/dev/vda console=ttyAMA0 oops=panic panic_on_warn=1 panic=-1 ftrace_dump_on_oops=orig_cpu debug earlyprintk=serial slab_debug=UZ" \
	-m 2048 \
	-net user,hostfwd=tcp::10023-:22 -net nic \
	$1 $2
