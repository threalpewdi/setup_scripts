#!/bin/bash

KERNEL="${HOME}/repos/kernels/linux/arch/x86/boot/bzImage"
QEMU_PATH="${HOME}/repos/emulators/qemu-2.12.0/i386-softmmu"
HDISK_PATH="${HOME}/repos/rootfs/x86_64/output/images"

${QEMU_PATH}/qemu-system-i386 \
	-kernel ${KERNEL} \
	-hda ${HDISK_PATH}/rootfs.ext2 \
	-curses \
	-boot c \
	-m 128 \
	-append "root=/dev/sda rw" \
	-serial stdio \
	$1 $2

