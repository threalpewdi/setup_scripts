
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <asm/thread_info.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("pewdi");
MODULE_DESCRIPTION("Test driver");

struct file_operations hello_fops;

static int __init hello_init(void)
{
	proc_create("hello", 0, NULL, &hello_fops);
	return 0;
}

static void __exit hello_exit(void)
{
	remove_proc_entry("hello", NULL);
}

static ssize_t hello_read(struct file *filp, char *buffer,
			  size_t length, loff_t *offset)
{
	printk("thread_info=%p\n", current_thread_info());
	return 0;
}

struct file_operations hello_fops = {
	.owner = THIS_MODULE,
	.read = hello_read,
};

module_init(hello_init);
module_exit(hello_exit);
