#!/bin/bash

set -e

apt_get_install() {
	echo "[+] Installing $1 using apt-get"
	sudo apt-get install -y $1
} &> /tmp/setuplog

packages=(
	vim
	vim-gtk
	curl
	git
	rake
	autojump
	xmonad
	xmobar
	suckless-tools
	libelf-dev
	libssl-dev
	xclip
	python
	qemu-kvm
	libvirt-bin
	virtinst
	bridge-utils
	libglib2.0-dev
	zlib1g-dev
	libpixman-1-dev
	flex
	bison
	python-pip
	virtualenv
	ufw
	g++
	zsh
	cscope
	exuberant-ctags
	openssh-server
)

echo "[+] Updating and upgrading"
sudo apt-get update &> /tmp/setuplog
sudo apt-get -y upgrade &> /tmp/setuplog

for package in "${packages[@]}"; do
apt_get_install $package
done

echo "[+] Generating ssh keys"
ssh-keygen &> /tmp/setuplog

echo "[+] Setting up git config"
git config --global user.name 'pewdie pie'
git config --global user.email 'threalpewdi@gmail.com'

echo "[+] Setting up vim carlhuda-janus"
(curl -L https://bit.ly/janus-bootstrap | bash) &> /tmp/setuplog

echo "[+] Setting up .vimrc"
cat > .vimrc.after << EOF
colorscheme xoria256
set clipboard=unnamedplus
set relativenumber
set cursorline
set ts=8 sts=8 sw=8 noet
set so=999
EOF

cat > .gvimrc.after << EOF
set guioptions-=m
set guioptions-=T
colorscheme solarized
set background=dark
set so=999
EOF

echo "[+] Configuring xmonad"
mkdir -p ~/bin
cat > ~/bin/xterm-noxim << EOF
#!/bin/sh
exec env XMODIFIERS=@im="" xterm
EOF

mkdir -p ~/.xmonad
cat > ~/.xmonad/xmonad.hs << EOF
import XMonad
import XMonad.Hooks.Script
import XMonad.Util.EZConfig
import XMonad.Layout.NoBorders
import XMonad.Config.Gnome

main = xmonad $ gnomeConfig {
	terminal = "xterm-noxim",
	layoutHook = smartBorders (layoutHook defaultConfig),
	startupHook = do
		execScriptHook "startup"
		spawn "/usr/bin/xcompmgr"
}
	\`additionalKeys\`
	[((mod1Mask), xK_p), spawn "dmenu_run")]
EOF

cat > ~/.xmonad/hooks << EOF
dbus-send --session --print-reply --dest=org.gnome.SessionManager
/org/gnome/SessionManager org.gnome.SessionManager.RegisterClient string:xmonad.desktop
string:$DESKTOP_AUTO_START_ID
EOF

chmod 755 ~/.xmonad/hooks ~/bin/xterm-noxim

echo "[+] Creating repos directory"
mkdir -p ~/repos

set +e
echo "[+] Setting up toolchains"
mkdir -p ~/repos/toolchains ; cd ~/repos/toolchains
wget https://releases.linaro.org/components/toolchain/binaries/latest/aarch64-linux-gnu/gcc-linaro-7.2.1-2017.11-x86_64_aarch64-linux-gnu.tar.xz &> /tmp/setuplog
wget https://releases.linaro.org/components/toolchain/binaries/latest/arm-eabi/gcc-linaro-7.2.1-2017.11-x86_64_arm-eabi.tar.xz &> /tmp/setuplog
tar -xf gcc-linaro-7.2.1-2017.11-x86_64_aarch64-linux-gnu.tar.xz
tar -xf gcc-linaro-7.2.1-2017.11-x86_64_arm-eabi.tar.xz
set -e
cd ~

echo "[+] Setting up kernel sources"
mkdir -p ~/repos/kernels ; cd ~/repos/kernels
git clone http://github.com/torvalds/linux.git &> /tmp/setuplog
git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git &> /tmp/setuplog
cd ~

echo "[+] Setting up rootfs"
mkdir -p ~/repos/rootfs ; cd ~/repos/rootfs
wget http://buildroot.org/downloads/buildroot-2018.02.2.tar.gz &> /tmp/setuplog
tar -xf buildroot-2018.02.2.tar.gz &> /tmp/setuplog ; mv buildroot-2018.02.2 x86_64
tar -xf buildroot-2018.02.2.tar.gz &> /tmp/setuplog ; mv buildroot-2018.02.2 arm
tar -xf buildroot-2018.02.2.tar.gz &> /tmp/setuplog ; mv buildroot-2018.02.2 aarch64
cd ~

echo "[+] Cloning self directory"
mkdir -p ~/repos/misc ; cd ~/repos/misc
git clone https://threalpewdi@bitbucket.org/threalpewdi/setup_scripts.git
bash -c 'cd setup_scripts; git remote rm origin'
bash -c 'cd setup_scripts; git remote add origin git@bitbucket.org:threalpewdi/setup_scripts.git'
cp setup_scripts/x86_64_br_config ~/repos/rootfs/x86_64/.config
cp setup_scripts/aarch64_br_config ~/repos/rootfs/aarch64/.config
cd ~

echo "[+] Getting QEMU kernel startup scripts"
mkdir -p ~/repos/rootfs/qemu_hds ; cd ~/repos/rootfs/qemu_hds
wget https://bitbucket.org/threalpewdi/setup_scripts/raw/4a0f9ccdb6e64dce2f076ac9f634a8482aa63e8a/x86_64_start.sh
# wget the startup scripts
cd ~

echo "[+] Getting kernel module skeletons"
mkdir -p ~/repos/kmodules

echo "[+] Setting up emulators"
mkdir -p ~/repos/emulators ; cd ~/repos/emulators
wget http://download.qemu.org/qemu-2.12.0.tar.xz &> /tmp/setuplog
tar -xf qemu-2.12.0.tar.xz &> /tmp/setuplog
cd qemu-2.12.0
./configure --enable-kvm &> /tmp/setuplog
echo "[+] -- Building QEMU"
make -j4 &> /tmp/setuplog
cd ~

echo "[+] Fetching gdb script"
wget -O ~/.gdbinit-gef.py -q https://github.com/hugsy/gef/raw/master/gef.py &> /tmp/setuplog
echo source ~/.gdbinit-gef.py >> ~/.gdbinit

echo "[+] Setting up zsh"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
chsh -s $(which zsh) &> /tmp/setuplog

echo "[+] Setting up .zshrc"
sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="kardan"/g' ~/.zshrc
cat >> ~/.zshrc << EOF
export EDITOR='vim'
export TERM=xterm-256color

export PATH=$PATH:~/repos/toolchains/gcc-linaro-7.2.1-2017.11-x86_64_aarch64-linux-gnu/bin
export PATH=$PATH:~/repos/toolchains/gcc-linaro-7.2.1-2017.11-x86_64_arm-eabi/bin

HISTFILE=~/.histfile
HISTSIZE=1000
HISTFILESIZE=2000

/usr/bin/setxkbmap -option 'ctrl:swapcaps'

alias get='xclip -selection clipboard'

source /usr/share/autojump/autojump.sh

function compress() {
  tar -zcvf "\$1.tar.gz" \$1
}

alias i='amixer -q set Master 10%+'
alias d='amixer -q set Master 10%-'
alias bat='upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -E "state|to\ full|percentage"'
EOF

echo "[+] Done setting up, enjoy.
