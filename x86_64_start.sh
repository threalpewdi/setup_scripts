#!/bin/bash

KERNEL="${HOME}/repos/kernels/linux/arch/x86_64/boot/bzImage"
QEMU_PATH="${HOME}/repos/emulators/qemu-2.12.0/x86_64-softmmu"
HDISK_PATH="${HOME}/repos/rootfs/x86_64/output/images"

${QEMU_PATH}/qemu-system-x86_64 \
	-kernel ${KERNEL} \
	-hda ${HDISK_PATH}/rootfs.ext2 \
	-boot c \
	-m 128 \
	-append "root=/dev/sda console=ttyS0 rw" \
	-serial stdio \
	$1 $2

